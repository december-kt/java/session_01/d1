package com.zuitt.example;

import java.util.Scanner;

public class UserInput {
    // String[] - array of strings
    // Default method of Java:
    public static void main(String[] args) {
        // We instantiate the myObj form the Scanner class
        // Scanner is used for obtaining input from the terminal.
        // "System.in" allows us to takes input from the console.
        Scanner myObj = new Scanner(System.in);
        // Prompt
        // System.out.println("Enter username ");
        // to capture the input given by the user, we will use the nextLine() method
        // takes and input until the line changes or a new line was initiated with enter.
        // String userName = myObj.nextLine();
        // System.out.println("Username is " + userName);
        // System.out.println(userName +userName);

        System.out.println("Enter a number to add: ");
        System.out.println("Enter first number: ");
        // Long Method:
        // int num1 = Integer.parseInt(myObj.nextLine());
        // Short Method:
        int num1 = myObj.nextInt();

        System.out.println("Enter second number: ");
        int num2 = myObj.nextInt();

        System.out.println("The sum of the two numbers is: " + (num1 + num2));

    }

}
